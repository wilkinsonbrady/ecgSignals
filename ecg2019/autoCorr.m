%## Copyright (C) 2019 Brady
%## 
%## This program is free software: you can redistribute it and/or modify it
%## under the terms of the GNU General Public License as published by
%## the Free Software Foundation, either version 3 of the License, or
%## (at your option) any later version.
%## 
%## This program is distributed in the hope that it will be useful, but
%## WITHOUT ANY WARRANTY; without even the implied warranty of
%## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%## GNU General Public License for more details.
%## 
%## You should have received a copy of the GNU General Public License
%## along with this program.  If not, see
%## <https://www.gnu.org/licenses/>.
%
%## -*- texinfo -*- 
%## @deftypefn {} {@var{retval} =} autoCorr (@var{input1}, @var{input2})
%##
%## @seealso{}
%## @end deftypefn
%
%## Author: Brady <Brady@BRADY-GPC>
%## Created: 2019-09-06

function [timeIndex, acOfSignal] = autoCorr (signal, sampleFreq, lag0)
  n = round(lag0.*sampleFreq);
  [acSignal, N]= xcorr(signal, n);
  N0length = length(N);
  Nlength = round(N0length/2):N0length;
  acOfSignal = acSignal(Nlength);
  timeIndex = N(Nlength)./sampleFreq;
end


%==============
%       Outputs
%==============
%timeIndex - time values for each data point
%acOfSignal - autocorrelation of the signal

%==============
%       Inputs
%==============
%signal - signal to be processed
%sampleFreq - sample frequency
%lag0 - time lag for autocorrelation in seconds











