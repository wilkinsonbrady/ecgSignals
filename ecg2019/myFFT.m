% ## Copyright (C) 2019 Brady
% ## 
% ## This program is free software: you can redistribute it and/or modify it
% ## under the terms of the GNU General Public License as published by
% ## the Free Software Foundation, either version 3 of the License, or
% ## (at your option) any later version.
% ## 
% ## This program is distributed in the hope that it will be useful, but
% ## WITHOUT ANY WARRANTY; without even the implied warranty of
% ## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% ## GNU General Public License for more details.
% ## 
% ## You should have received a copy of the GNU General Public License
% ## along with this program.  If not, see
% ## <https://www.gnu.org/licenses/>.
% 
% ## -*- texinfo -*- 
% ## @deftypefn {} {@var{retval} =} myFFT (@var{input1}, @var{input2})
% ##
% ## @seealso{}
% ## @end deftypefn
% 
% ## Author: Brady <Brady@BRADY-GPC>
% ## Created: 2019-09-05

function ff = myFFT (signal, sampleFreq)
  ffp = fft(signal, sampleFreq); %fft of ac1OfSig1
  nffp = length(ffp);
  ff = ffp(1:nffp/2); %only half the data needed because its mirrored
end

%==============
%       Outputs
%==============
%ff - fft of the data

%==============
%       Inputs
%==============
%signal - data to be processed
%sampleFreq - sample frequency

