%## Copyright (C) 2019 Brady
%## 
%## This program is free software: you can redistribute it and/or modify it
%## under the terms of the GNU General Public License as published by
%## the Free Software Foundation, either version 3 of the License, or
%## (at your option) any later version.
%## 
%## This program is distributed in the hope that it will be useful, but
%## WITHOUT ANY WARRANTY; without even the implied warranty of
%## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%## GNU General Public License for more details.
%## 
%## You should have received a copy of the GNU General Public License
%## along with this program.  If not, see
%## <https://www.gnu.org/licenses/>.
%
%## -*- texinfo -*- 
%## @deftypefn {} {@var{retval} =} peakDetect (@var{input1}, @var{input2})
%##
%## @seealso{}
%## @end deftypefn
%
%## Author: Brady <Brady@BRADY-GPC>
%## Created: 2019-09-19


%Current Method:
%   1. Low Pass butterworth Filter (100Hz = Fc)
%   2. Derivative filter - Removes DC offset as well
%   3. Squaring Function
%   4. Remove Amplitudes below the average height
%   5. Find peaks
%   6. Remove negative peaks
%   7. Remove any values that are within 20% of the average width between signal - may need to be done via a different method
%   8. Overlap check - Take a window at each peak that is 70% of the average width between signals and find the maximum value
%                                  in that window - checked against signal with Derivative and lowpass filter applied
%   9. False value check - Remove peaks that are under 50% of the average peak amplitude - checked against signal with Derivative and lowpass filter applied



function [Rf] = peakDetect (sig, fs, dispPeaks = false)
  l = 20;
  K = 20;
  h = lowdiff(K,l);
  L = round((20/1000)*fs);

   %highpass butterworth
  sigN = myFilt(sig,4,5,fs);
  
  f = zeros(L,1)+1;
  sigNa = filter(f, 1, sigN);

%  %Remove 60 and 120Hz noise
%    [B,A] = pei_tseng_notch([60/(fs/2),120/(fs/2)], [10/(fs/2),10/(fs/2)]);
%    sigN = filter(B,A,sigN);
    
  %Derivative filter
  sigNd = filtfilt(h,1,sigNa);
  
  %Square  Function
  d = (sigNd).^2;    
    
  %Gets rid of values below the avg
  da = sum(d)/length(d);
  d = d-da;
  d = sign(sign(d)+1).*d;
  

  
  
  %Find peaks
  [p, R] = findpeaks(d);
  
  
  
  %Gets rid of negative peaks
%  R = R';
%  R = nonzeros(R);
%  R = sign(sign(sigNa(R))+1).*R;
%  R = nonzeros(R);
  
  

    %Remove values within 20% of the average width
  Rd(1:length(R)) = 0;
  Rd(2:end) = R(2:end)-R(1:end-1);
  Rda = sum(Rd)/length(Rd);
  R = sign((sign(Rd'-Rda*.2)+1)).*R;
  R = nonzeros(R);


%Overlap Check
counter = 1;
checkW = round(Rda*.8);
  R = unique(R);
  for i = R'
    if 1 < i-checkW && length(sigN) > i+checkW %check window is within the bounds
      cv = sigN(i-checkW:i+checkW);
      wOffset = checkW;
    elseif 1 > i-checkW %check window goes past the 1st index
      cv = sigN(1:i+checkW);
      wOffset = 1;
     else %check window is at the end of the vector
      cv = sigN(i-checkW:end);
      wOffset = length(sigN)-checkW;
    end
    [value, offset(counter)] = max(cv); %find maximum value in window
    Rf(counter) = i+(offset(counter)-wOffset); % store value and account for offset
    counter++;
  end
  
   %Remove values within 20% of the average width
  Rfd(1:length(Rf)) = 0;
  Rfd(2:end) = Rf(2:end)-Rf(1:end-1);
  Rfda = sum(Rfd)/length(Rfd);
  Rf = sign((sign(Rfd-Rfda*.2)+1)).*Rf;
  Rf = nonzeros(Rf);
  
  %Remove peaks under 50% of average peak value
  pa = sum(sigN(Rf))./length(Rf);
  Rf = sign(sign(sigN(Rf)-pa.*.5)+1).*Rf';
  Rf = nonzeros(Rf);
  Rf = unique(Rf);
  
  if dispPeaks == true
    figure
    plot(sig)
    hold on
    stem(Rf,sig(Rf))
    title('Detected Peaks')
    for i = 1:length(Rf)
      wLabel = sprintf('%d', i);
      y = max(sig)+0.1;
      text(Rf(i), y, wLabel, 'HorizontalAlignment', 'center');
    end
  end
end

%http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.167.550&rep=rep1&type=pdf














