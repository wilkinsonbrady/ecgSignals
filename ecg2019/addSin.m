## Copyright (C) 2019 Brady
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} addSin (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Brady <Brady@BRADY-GPC>
## Created: 2019-09-05

function sig = addSin (signal, sampleFreq, f, amplitude=1, phase=0)
  T = 1/sampleFreq;
  t = 0:T:((length(signal)-1) .* T);
  sin1 = amplitude.*sin(f .*2 .*pi.*t+phase);                       %sin(wt+theta)
  sig = signal  + sin1';
end

%==============
%       Outputs
%==============
%sig - processed signal

%==============
%       Inputs
%==============
%signal - signal to be processed
%sampleFreq - sample frequency
%f - frequency to add in
%amplitude - amplitude of the sine wave to add in
%phase - phase shift of the sine wave







