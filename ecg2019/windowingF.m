% ## Copyright (C) 2019 Brady
% ## 
% ## This program is free software: you can redistribute it and/or modify it
% ## under the terms of the GNU General Public License as published by
% ## the Free Software Foundation, either version 3 of the License, or
% ## (at your option) any later version.
% ## 
% ## This program is distributed in the hope that it will be useful, but
% ## WITHOUT ANY WARRANTY; without even the implied warranty of
% ## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% ## GNU General Public License for more details.
% ## 
% ## You should have received a copy of the GNU General Public License
% ## along with this program.  If not, see
% ## <https://www.gnu.org/licenses/>.
% 
% ## -*- texinfo -*- 
% ## @deftypefn {} {@var{retval} =} windowingF (@var{input1}, @var{input2})
% ##
% ## @seealso{}
% ## @end deftypefn
% 
% ## Author: Brady <Brady@BRADY-GPC>
% ## Created: 2019-09-06


%QRS window size 
%Rejecting Data using correlation

function [windowValue, afftw, ffw, ffwSquare, c2AvgFFT, c2AvgT, windowEndPoints, windowIndexed, averageWaveform, windowFinalIndexedT, keptPeaksT, removedPeaksT, avgFinalFFT, averageWaveformTFFT] = windowingF (signal, sampleFreq, windowWidth, windowCenter, showWindows = false, numToAvg=15, numOfZeros = 0)
    windowWidthIndex = windowWidth*sampleFreq; %converts window size in sec to N
    if showWindows == true
      figure
      title('Windowed Values')
      hold on
      plot(signal)
    end
    
     prev = 1;
     curr = 0;
      zs = zeros(numOfZeros,1);
     
     
     
    for index = 1:length(windowCenter)                                                   %makes sure N values in each window are not less than 1.... the IF statement really only affects first window
      if (windowCenter(index) - ceil(windowWidthIndex/2) < 1)              %centers window around R peaks
%        window = 1:(windowCenter(index) + ceil(windowWidthIndex/2));  %doesn't yet account for if window is greater than the range of values
        continue;
      elseif  windowCenter(index) + ceil(windowWidthIndex/2) > length(signal)
        continue;
      else                                                                          %but shouldn't matter because it shouldn't be much larger than QRS
        window = (windowCenter(index) - round(windowWidthIndex/2)):(windowCenter(index) + ceil(windowWidthIndex/2)) ; %sets window from (R peak - half the window) to (R peak + half the window)
      end
      if curr - numToAvg < 0
        numToAvg = 0;
      end
      curr = curr+length(window)+length(zs)*2; %current counter value
      counter = prev:curr; %which part of window the values are in... basically concatenates the data to one another
      wcounter = length(counter)-length(zs)*2-1;
      hCounter = 0:wcounter;
      windowValue(counter) = [zs; signal(window); zs]; 
      
      offset = ((sum(windowValue((curr-numToAvg):curr))/(numToAvg+1))+ (sum(windowValue(prev:(prev+numToAvg)))/(numToAvg+1)))/2; %average the endpoints and account for baseline drift
      windowValue(counter) = windowValue(counter)-offset; %factors in offset
      




      windowingFunction1 = 0.5 .* (1-cos(((2 .* pi .* hCounter)./length(hCounter)))); %Hann Window

%       a = 2;
%      windowingFunction1 =  0.5 .* (1-cos((2.*pi.*hCounter)./length(hCounter))).*e.^((-a.*abs(length(hCounter)-2.*hCounter))./length(hCounter)); %Hann-Poisson
      
%      a = 0.16;
%      a0 = (1-a)./2;
%      a1 = 0.5;
%      a2 = a./2;
%      windowingFunction1 = a0 - a1.*cos((2.*pi.*hCounter)./(length(hCounter)-1))+ a2.*cos((4.*pi.*hCounter)./(length(hCounter)-1)); %Blackman window

%            windowingFunction1 = 1; %Rectangular window

      windowingFunction = [zs' windowingFunction1 zs'];


      windowValue(counter) = windowingFunction  .* windowValue(counter); %window function - Hann window, since window length doesn't change could be moved
      windowEndPoints(index) = curr; %locations for the endpoints of each window within the concatenated data                  https://www.mathworks.com/help/signal/ref/hann.html;jsessionid=b377bde3fe52d15699a8b58cea04
      
      windowIndexed(index,:) = windowValue(counter); %windowValue in seperate rows
      
      if showWindows == true
      windowXL = window(1);
      windowXU = window(end);
      wLabel = sprintf('%d', index);
      text(windowXL, max(signal)+0.1, wLabel, 'HorizontalAlignment', 'center');
      line([windowXL windowXL],[min(signal) max(signal)] , 'linewidth', 2, 'color', [1, 0, 0]); %draw lines between QRS
      line([windowXU windowXU],[min(signal) max(signal)] , 'linewidth', 2, 'color', [1, 0, 0]); %draw lines between QRS
    end
    
      ffw(index,:)=myFFT(windowValue(prev:curr),sampleFreq); %fft of signal
      ffwSquare(index,:)=abs(ffw(index,:)).^2;
      prev = prev+length(window)+length(zs)*2; %keeps track of prev counter value
      
    end
    tfftw = sum(abs(ffw).^2);
    afftw=tfftw./index; %average fft mag^2 of window
    
    twV = sum(windowIndexed);
    averageWaveform = twV./index; %average waveform
    
    %Correlation in both the frequency and the time domain
    for i = 1:size(ffw,1)
        c2AvgFFT(i) = corr(abs(ffwSquare(i,:)), abs(afftw));
    end
    
     for i = 1:size(windowIndexed,1)
        c2AvgT(i) = corr(averageWaveform, windowIndexed(i,:));
    end
    
    indexKeep = 1;
    indexRemove = 1;
    removedFlag = false;
    for i = 1:length(c2AvgT)
      if c2AvgT(i) > 0.8
        keptWindows(indexKeep++) = i;
      else
        removedWindows(indexRemove++) = i;
        removedFlag = true;
      end
    end
    
     i = 0;
      for index = 1:length(keptWindows)
         if (keptWindows(index) - ceil(windowWidthIndex/2) < 1)              %centers window around R peaks
          window = 1:(windowCenter(index) + ceil(windowWidthIndex/2));  %doesn't yet account for if window is greater than the range of values 
         else                                                                          %but shouldn't matter because it shouldn't be much larger than QRS
            window = (keptWindows(index) - round(windowWidthIndex/2)):(keptWindows(index) + ceil(windowWidthIndex/2)) ; %sets window from (R peak - half the window) to (R peak + half the window)
         end
         
         if index != 1
           if window(1) > winRange 
             i++;
             if  c2AvgT(index) > c2AvgT(index-1)
               keptWindows(i) = keptWindows(index);
             else
               keptWindows(i) = keptWindows(index-1);
             end
           end
           winRange = window(end);
         else
           winRange = window(end);
      end
     end
     keptWindows = unique(keptWindows);
      
      
      windowFinalIndexedT = windowIndexed(keptWindows,:);
      keptPeaksT = windowCenter(keptWindows);
      if removedFlag == true
        removedPeaksT = windowCenter(removedWindows);
      else
        removedPeaksT = NaN;
      end
      
      
      
      
      
%      finalFFTAvg = 0 .* (1:size(windowFinalIndexedT,2));      
%      finalTAvg = 0 .* (1:size(windowFinalIndexedT,2));      
%      for i = 1:size(windowFinalIndexedT,2)
%        tempFFT = abs(myFFT(windowFinalIndexedT(i,:), sampleFreq)).^2;
%        finalFFTAvg = finalFFTAvg + tempFFT;
%        finalTAvg = finalTAvg +  windowFinalIndexedT(i,:);
%      end
%      finalFFTAvg = finalFFTAvg./size(finalFFTAvg,2);
%      finalTAvg = finalTAvg./size(finalTAvg,2);
%      finalTAvgFFT = abs(myFFT(finalTAvg, sampleFreq)).^2;
      
      
    FinalFFTTot = sum(abs(ffw(keptWindows,:)).^2);
    avgFinalFFT=FinalFFTTot./size(keptWindows,2); %average fft mag^2 of window
    
    avgWaveFinalTot = sum(windowFinalIndexedT);
    averageWaveformTFinal = avgWaveFinalTot./size(windowFinalIndexedT,1); %average waveform
    averageWaveformTFFT = abs(myFFT(averageWaveformTFinal, sampleFreq)).^2;
      
      
      
%      figure
%      hold
%      semilogy(avgFinalFFT)
%      semilogy(averageWaveformTFFT)
%      legend('FFT from Windows', 'Avg waveform FFT');
%      title('With peaks removed');
end


%==============
%       Outputs
%==============
%windowValue - data after being windowed
%afftw - average magnitude squared of fft
%ffw - fft of each window in an array
%windowEndPoints - locations in the data for the end of each windowed part
%c2AvgFFT - Correlation of each window to the average in the Frequency domain
%c2AvgT - Correlation of each window to the average in the time domain
%windowIndexed - the windowed function with each window in seperate indeces
%averageWaveform - average waveform

%==============
%       Inputs
%==============
%signal - signal to be windowed
%sampleFreq - sampling frequency
%windowWidth - how wide the window needs to be in seconds
%windowCenter - center point for each window... may change this so that you can do different lengths from either side of this "center" point
%numToAvg - number of indeces to go in when taking out offset



