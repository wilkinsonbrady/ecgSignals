function [sig,b,a]= myFilt(n, o, Fc,Fs)
  [b,a] = butter(o, Fc./(Fs./2),'high');
  sig = filter(b,a,n);
  
%  figure(101);
%  [h,w] = freqz(b,a);
%  freqz_plot(w, h);
  
end

%==============
%       Outputs
%==============
%sig - filtered signal
%b - b coefficients for the filter
%a - a coefficients for the filter

%==============
%       Inputs
%==============
%n - signal to be filtered
%o - number of coeffiecients used in the filter
%Fc - cutoff frequency
%Fs - sampling frequency

